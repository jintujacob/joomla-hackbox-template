<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.hackbox
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$user            = JFactory::getUser();
$this->language  = $doc->language;
$this->direction = $doc->direction;

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');


?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xml:lang="<?php echo $this->language; ?>"
	lang="<?php echo $this->language; ?>"
	dir="<?php echo $this->direction; ?>">
	
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jdoc:include type="head" />
	
	<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/hackbox/css/template.css" type="text/css" />
	<style type="text/css">
	</style>
</head>

<body>
	<div id="container">
		<div id="header">
			<jdoc:include type="modules" name="top" /> 
		</div>
		<div id="sidebar_left" class="float">
			<jdoc:include type="modules" name="left" />
		</div>
		<div id="content" class="float">
			<jdoc:include type="component" />
		</div>
		<div id="sidebar_right" class="float">
			<jdoc:include type="modules" name="right" />
		</div>
		<div id="footer" class="clear">
			<jdoc:include type="modules" name="footer" />
		</div>
	</div>

</body>
</html>
